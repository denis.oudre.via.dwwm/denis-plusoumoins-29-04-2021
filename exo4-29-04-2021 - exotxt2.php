<?php

$a = "Du TexTe aVEc des Majuscules ET des MINUsculES Un peu N'IMPORTE Où !";
//
//// écrire le code permettant de retirer toutes les majuscules et de n'avoir plus que des minuscules dans le texte
//// contenu dans la variable $a en utilisant la fonction strtolower()
//// https://www.php.net/manual/fr/function.strtolower.php

$text = strtolower($a);
echo $text;
