<?php


/*
 * - Créer une variable nombreAtrouver et lui affecter une valeur aléatoire
 * - afficher "choisir un nombre"
 * - stocker la réponse dans la variable réponse
 * - Si réponse < nombreAtrouver
 *  afficher "c'est plus"
 * - sinon si réponse > nombreAtrouver
 *  afficher "c'est moins"
 * - sinon
 *  affiché c'est gagné
 *  jouer de la musique
 *
 */

$nombreAtrouver = mt_rand(0, 100);

// echo $nombreAtrouver . "nombre à trouver";
echo "Choisir un nombre :";
$reponse = intval(readline());


while ($nombreAtrouver != $reponse) {
    if ($reponse < $nombreAtrouver) {
        echo "C'est plus !\n";
    } else if ($reponse > $nombreAtrouver) {
        echo "C'est moins !\n";
    }
    $reponse = readline("Choisir un nombre : ");
}
echo "Bravo vous avez trouvé ! \n";