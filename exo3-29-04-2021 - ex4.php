<?php
//## 4 - nombre premier
//demander un nombre à l'utilisateur
//indiquer si ce nombre est premier (https://fr.wikipedia.org/wiki/Nombre_premier)

$premier = 1;
$nbdiv = 0;

echo "Donnez moi un nombre :";
$reponse = (int)readline();

for($i=1; $i<=$reponse; $i++){

    $rest=$reponse%$i;
    if($rest == 0) {
        $nbdiv++;
    }
}

if($nbdiv == 2){
    echo "c'est un nombre premier\n";
}
else {
    echo "c'est pas un nombre premier\n";
}

