<?php

$a = "Ma couleur préférée est le vert!";

// écrire le code permettant de remplacer le mot "vert" par le mot "rouge" dans le texte
// contenu dans la variable $a en utilisant la fonction str_replace()
// https://www.php.net/manual/en/function.substr-replace.php

$b = str_replace("vert","rouge",$a);
echo $b;