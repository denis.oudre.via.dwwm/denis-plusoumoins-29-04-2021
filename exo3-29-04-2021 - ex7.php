<?php
//7
//afficher les nombres de la suite de Fibonacci inferieurs à 5000 (https://fr.wikipedia.org/wiki/Suite_de_Fibonacci)

$f0 = 0;
$f1 = 1;
$ft = 0;

echo $f0." ".$f1." ";

while ($ft < 4100) {

    $ft = $f0 + $f1;
    echo $ft." ";
    $f0 = $f1;
    $f1 = $ft;
}
echo "\n";
