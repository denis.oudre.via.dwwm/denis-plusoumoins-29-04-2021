<?php

$a = "Je+suis+une+cha%C3%AEne+de+caract%C3%A8res+%21";

// écrire le code permettant de décoder la chaîne de caractères encodée
// utiliser la fonction urldecode de manière à décoder la chaîne de caractères :
// https://www.php.net/manual/fr/function.urldecode

$b = urldecode($a);
echo $b;