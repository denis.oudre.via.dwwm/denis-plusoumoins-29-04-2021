<?php
//8
//afficher les 20 premiers nombres premiers
// (https://fr.wikipedia.org/wiki/Nombre_premier)

$cpt = 1;
$n = 2;
$nbdiv = 0;

while ($cpt < 21){
    for($i=1; $i<=$n; $i++){

        $rest=$n%$i;
        if($rest == 0) {
            $nbdiv++;
        }
    }
    if($nbdiv == 2) {
        echo $cpt . " " . $n . "\n";
        ++$cpt;
    }
    $nbdiv = 0;
    ++$n;
}




