<?php

$a = "12345 ";

// écrire le code permettant de répéter la chaine de caractères trois fois
// utiliser la fonction str_repeat de manière à ce que le contenu de la chaine de caractères soit répété:
// https://www.php.net/manual/fr/function.str-repeat.php

$b = str_repeat($a,3);
echo $b;