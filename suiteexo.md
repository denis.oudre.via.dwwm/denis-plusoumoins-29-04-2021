
$a = "  Bonjour le monde     ";

// écrire le code permettant de supprimer les espaces contenus de chaque côté de la chaine de caractères
// utiliser la fonction trim :
// https://www.php.net/manual/fr/function.trim.php

$a = "Du TexTe aVEc des Majuscules ET des MINUsculES Un peu N'IMPORTE Où !";


//--------------------------------

// écrire le code permettant de retirer toutes les majuscules et de n'avoir plus que des minuscules dans le texte
// contenu dans la variable $a en utilisant la fonction strtolower()
// https://www.php.net/manual/fr/function.strtolower.php


//--------------------------------

$seriousText ="We have to leverage up the messaging locked and loaded we should leverage existing asserts that ladder up to the message for put in in a deck for our standup today this vendor is incompetent . Drive awareness to increase engagement. Make sure to include in your wheelhouse big picture but big data market-facing finance nor manage expectations that's mint, well done. Wheelhouse wiggle room, nor today shall be a cloudy day, thanks to blue sky thinking, we can now deploy our new ui to the cloud yet low hanging fruit. Drink the Kool-aid out of the loop, and get all your ducks in a row, but we need to dialog around your choice of work attire. What's the status on the deliverables for eow? teams were able to drive adoption and awareness for downselect for keep it lean. Work flows rehydrate the team. To be inspired is to become creative, innovative and energized we want this philosophy to trickle down to all our stakeholders moving the goalposts. Turn the ship big boy pants, (let's not try to) boil the ocean (here/there/everywhere) it is all exactly as i said, but i don't like it optics Q1, so optics. Not enough bandwidth we want to see more charts quick sync, but can we take this offline make it more corporate please create spaces to explore what’s next. Mobile friendly window of opportunity guerrilla marketing.";
$count = 0
// Écrire le code permettant de compter le nombre de lettres dans ce paragraphe.
// stocker le nombre de caractères dans la variable $count en utilisant la fonction strlen()
// https://www.php.net/manual/en/function.strlen.php


//--------------------------------

$deliciousText = "Jujubes toffee danish chocolate cake. Chocolate cake muffin cake topping cupcake. Donut sweet roll jelly-o chocolate cake. Brownie powder caramels sweet I love. Croissant caramels jelly marzipan croissant. Lollipop halvah cake gummi bears chocolate cake. Gummies ice cream chupa chups. Dessert bonbon I love jelly-o tootsie roll marzipan gummies cotton candy. Bear claw jelly-o muffin ice cream. Gummi bears apple pie apple pie. Bonbon marshmallow cake I love cupcake liquorice. Carrot cake lollipop muffin bonbon jelly beans powder. Tart topping candy canes sweet marzipan sugar plum pudding bonbon.
Biscuit tiramisu jelly beans jelly chocolate cake donut bear claw I love toffee. Jelly beans muffin marzipan. Jelly-o ice cream tootsie roll. Apple pie icing I love carrot cake marzipan cake fruitcake marshmallow brownie. Dragée soufflé muffin danish I love. Jelly-o cake toffee. Cake caramels cake sweet roll. Powder lemon drops marzipan. Dessert gummi bears cupcake. Sweet roll wafer wafer jujubes. Bear claw I love marshmallow croissant candy tootsie roll. Halvah wafer tiramisu topping I love I love.";
$count = 0;
// écrire le code permettant de compter le nombre d'occurrences du MOT "cake" dans le texte
// stocker le résultat dans la variable $count en utilisant la fonction substr_count()
// https://www.php.net/manual/en/function.substr-count.php


//--------------------------------

$a = "Ma couleur préférée est le vert!";

// écrire le code permettant de remplacer le mot "vert" par le mot "rouge" dans le texte
// contenu dans la variable $a en utilisant la fonction str_replace()
// https://www.php.net/manual/en/function.substr-replace.php


//--------------------------------

$a = "12345 ";

// écrire le code permettant de répéter la chaine de caractères trois fois
// utiliser la fonction str_repeat de manière à ce que le contenu de la chaine de caractères soit répété:
// https://www.php.net/manual/fr/function.str-repeat.php


//--------------------------------

$a = "Je+suis+une+cha%C3%AEne+de+caract%C3%A8res+%21";

// écrire le code permettant de décoder la chaîne de caractères encodée
// utiliser la fonction urldecode de manière à décoder la chaîne de caractères :
// https://www.php.net/manual/fr/function.urldecode


//--------------------------------

$a = "<p>Non !</p> <p>Vive les <strong>chats</strong> !</p>";

// écrire le code permettant de supprimer les balises HTML de la chaîne de caractères
// utiliser la fonction strip_tags de manière à supprimer les balises HTML :
// https://www.php.net/manual/fr/function.strip-tags