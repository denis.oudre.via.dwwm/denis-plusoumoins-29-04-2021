<?php

$a = "<p>Non !</p> <p>Vive les <strong>chats</strong> !</p>";

// écrire le code permettant de supprimer les balises HTML de la chaîne de caractères
// utiliser la fonction strip_tags de manière à supprimer les balises HTML :
// https://www.php.net/manual/fr/function.strip-tags

$b = strip_tags($a);
echo $b;